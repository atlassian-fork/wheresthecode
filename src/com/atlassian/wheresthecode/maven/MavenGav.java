package com.atlassian.wheresthecode.maven;

import com.atlassian.fugue.Option;

public class MavenGav {
    private final Option<String> groupId;
    private final String artifactId;
    private final Option<String> version;

    public MavenGav(Option<String> groupId, String artifactId, Option<String> version) {
        this.groupId = groupId;
        this.artifactId = artifactId;
        this.version = version;
    }
    public MavenGav(String groupId, String artifactId, String version) {
        this(Option.option(groupId), artifactId, Option.option(version));
    }

    public Option<String> getGroupId() {
        return groupId;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public Option<String> getVersion() {
        return version;
    }
}
