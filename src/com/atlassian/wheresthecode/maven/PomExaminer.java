package com.atlassian.wheresthecode.maven;

import com.atlassian.fugue.Option;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.InputStream;

/**
 * Pulls apart the XML of a pom.xml for key information
 */
public class PomExaminer {

    /**
     * Reads the GAV for a pom.xml from the input
     *
     * @param pomDotXml the input stream  of a pom.xml
     * @return an optional GAV
     */
    public Option<MavenProject> extractProject(InputStream pomDotXml) {
        Document pom = getDoc(pomDotXml);

        Option<String> groupId = findPomElement(pom, "/project/groupId");
        Option<String> artifactId = findPomElement(pom, "/project/artifactId");
        Option<String> version = findPomElement(pom, "/project/version");

        Option<MavenGav> parentGav = extractParentGav(pom);
        Option<MavenScmInfo> scmInfo = extractScm(pom);

        if (artifactId.isDefined()) {
            return Option.some(new MavenProject(
                    new MavenGav(groupId, artifactId.get(), version),
                    parentGav,
                    scmInfo
            ));
        }
        return Option.none();
    }

    private Option<MavenGav> extractParentGav(Document pom) {
        Option<String> groupId = findPomElement(pom, "/project/parent/groupId");
        Option<String> artifactId = findPomElement(pom, "/project/parent/artifactId");
        Option<String> version = findPomElement(pom, "/project/parent/version");

        if (artifactId.isDefined()) {
            return Option.some(new MavenGav(groupId, artifactId.get(), version));
        }
        return Option.none();
    }


    private Option<MavenScmInfo> extractScm(Document pom) {
        Option<String> connnection = findPomElement(pom, "/project/scm/connection");
        Option<String> url = findPomElement(pom, "/project/scm/url");

        if (connnection.isDefined() || url.isDefined()) {
            return Option.some(new MavenScmInfo(connnection, url));
        }
        return Option.none();
    }

    private Option<String> findPomElement(Document pom, String path) {
        Node node = xpath(pom, path);
        if (node != null) {
            return Option.some(node.getTextContent());
        }
        return Option.none();
    }

    private Node xpath(Document pom, String path) {
        try {
            //noinspection unchecked
            XPath xpath = XPathFactory.newInstance().newXPath();
            XPathExpression xPathExpression = xpath.compile(path);
            return (Node) xPathExpression.evaluate(pom, XPathConstants.NODE);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Document getDoc(InputStream pomDotXml) {
        try {
            DocumentBuilder parser =
                    DocumentBuilderFactory.newInstance().newDocumentBuilder();
            return parser.parse(pomDotXml);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
