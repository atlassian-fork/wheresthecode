package com.atlassian.wheresthecode.repo;

import com.atlassian.fugue.Option;
import com.atlassian.wheresthecode.maven.MavenGav;
import com.atlassian.wheresthecode.maven.MavenProject;
import com.atlassian.wheresthecode.maven.MavenScmInfo;
import com.atlassian.wheresthecode.maven.PomExaminer;
import com.atlassian.wheresthecode.repo.finder.DiskPomFinder;
import com.atlassian.wheresthecode.repo.finder.FindResult;
import com.atlassian.wheresthecode.repo.finder.VfsPomFinder;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.option;

public class RepoFinder {

    private final VfsPomFinder vfsPomFinder;
    private final PomExaminer pomExaminer;
    private final DiskPomFinder diskPomFinder;
    private final RepoInfoFormatter repoInfoFormatter;

    public RepoFinder() {
        vfsPomFinder = new VfsPomFinder();
        pomExaminer = new PomExaminer();
        diskPomFinder = new DiskPomFinder();
        repoInfoFormatter = new RepoInfoFormatter();
    }

    public Option<RepoInfo> findRepo(Project project, VirtualFile startFile) {

        boolean searchM2Repo = false;

        VirtualFile virtualFile = startFile;
        MavenGav gav = null;
        while (true) {
            FindResult result;
            if (searchM2Repo) {
                result = diskPomFinder.findPomDotXml(gav);
            } else {
                result = vfsPomFinder.findPomDotXml(project, virtualFile);
                searchM2Repo = result.isInLibraryCode();
            }
            if (!result.isDefined()) {
                break;
            }
            Option<MavenProject> mavenProjectOpt = pomExaminer.extractProject(result.getPomDotXml().get());
            if (mavenProjectOpt.isEmpty()) {
                break;
            }
            Option<MavenScmInfo> scmInfo = mavenProjectOpt.flatMap(MavenProject::getScmInfo);
            if (scmInfo.isDefined()) {
                return Option.some(repoInfoFormatter.format(mavenProjectOpt.get(), scmInfo.get()));
            }
            //
            // ok its not in this pom, go up to find it in the VFS if we can
            if (!searchM2Repo) {
                virtualFile = virtualFile.getParent();
                if (option(virtualFile).isEmpty()) {
                    break;
                }
            } else {

                if (mavenProjectOpt.get().getParentGav().isEmpty()) {
                    break;
                }
                gav = mavenProjectOpt.get().getParentGav().get();

                searchM2Repo = true;
            }
        }
        return none();
    }

}
