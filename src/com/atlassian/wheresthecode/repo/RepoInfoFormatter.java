package com.atlassian.wheresthecode.repo;

import com.atlassian.fugue.Option;
import com.atlassian.wheresthecode.maven.MavenGav;
import com.atlassian.wheresthecode.maven.MavenProject;
import com.atlassian.wheresthecode.maven.MavenScmInfo;
import com.google.common.collect.ImmutableMap;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.wheresthecode.repo.RepoInfo.RepoType.*;

public class RepoInfoFormatter {

    private static final ImmutableMap<String, RepoInfo.RepoType> SCM_TYPES = ImmutableMap.<String, RepoInfo.RepoType>builder()
            .put("scm:git:", GIT)
            .put("scm:svn:", SVN)
            .put("scm:cvs:", CVS)
            .put("scm:hg:", HG)
            .put("scm:missing:", MISSING)
            .build();
    private static final Pattern scmRawAddressPattern = Pattern.compile("^(scm):([a-zA-z0-9]+):(.*)");
    private static final Pattern scmAddressPattern = Pattern.compile("^([a-zA-Z]*://)(.*)");

    public RepoInfo format(MavenProject mavenProject, MavenScmInfo scmInfo) {
        ScmParsed scmParsed = parse(scmInfo);

        RepoInfo.RepoType type = scmParsed.type;
        String cloneCommand = makeCloneCmd(scmParsed);
        String address = scmParsed.address;
        String url = scmInfo.getUrl().getOrNull();
        RepoInfo.Gav gav = makeGav(mavenProject);
        return new RepoInfo(type, gav, cloneCommand, address, url);
    }

    private RepoInfo.Gav makeGav(MavenProject mavenProject) {
        MavenGav childGav = mavenProject.getGav();
        Option<MavenGav> parentGav = mavenProject.getParentGav();
        String artefactId = childGav.getArtifactId();
        String groupId = "Not Known";
        String version = "Not Known";
        if (childGav.getGroupId().isDefined()) {
            groupId = childGav.getGroupId().get();
        } else if (parentGav.isDefined() && parentGav.get().getGroupId().isDefined()) {
            groupId = parentGav.get().getGroupId().get();
        }
        if (childGav.getVersion().isDefined()) {
            version = childGav.getVersion().get();
        } else if (parentGav.isDefined() && parentGav.get().getVersion().isDefined()) {
            version = parentGav.get().getVersion().get();
        }

        return new RepoInfo.Gav(groupId, artefactId, version);
    }

    private String makeCloneCmd(ScmParsed scmParsed) {
        if (scmParsed.type == GIT) {
            return "git clone " + scmParsed.address;
        } else if (scmParsed.type == SVN) {
            return "svn checkout " + scmParsed.address;
        } else if (scmParsed.type == CVS) {
            return "cvs checkout " + scmParsed.address;
        } else if (scmParsed.type == HG) {
            return "hg clone " + scmParsed.address;
        } else {
            return "I don't know how to clone this code";
        }
    }

    private String normaliseConnection(MavenScmInfo scmInfo) {
        return scmInfo.getConnnection().getOrElse("scm:missing:The SCM connection is not defined in the pom.xml").trim();
    }

    private ScmParsed parse(MavenScmInfo scmInfo) {
        ScmParsed scmParsed = new ScmParsed();
        String connection = normaliseConnection(scmInfo);
        scmParsed.type = OTHER;
        for (String typeKey : SCM_TYPES.keySet()) {
            if (connection.startsWith(typeKey)) {
                scmParsed.type = SCM_TYPES.get(typeKey);
                break;
            }
        }
        scmParsed.rawAddress = rawAddressOf(connection);
        scmParsed.address = addressOf(scmParsed.rawAddress);
        scmParsed.rawAddress = rawAddressOf(connection);
        scmParsed.address = addressOf(scmParsed.rawAddress);
        return scmParsed;
    }

    private String rawAddressOf(String connection) {
        Matcher matcher = scmRawAddressPattern.matcher(connection);
        if (matcher.find()) {
            MatchResult result = matcher.toMatchResult();
            if (result.groupCount() >= 3) {
                return result.group(3);
            }
        }
        return connection;
    }

    private String addressOf(String rawAddress) {
        // take off any ssh:// kinda of thing
        if (rawAddress.contains("://")) {
            Matcher matcher = scmAddressPattern.matcher(rawAddress);
            if (matcher.find()) {
                MatchResult result = matcher.toMatchResult();
                if (result.groupCount() >= 2) {
                    return result.group(2);
                }
            }
        }
        return rawAddress;
    }

    private class ScmParsed {
        RepoInfo.RepoType type;
        String rawAddress;
        String address;
    }
}
