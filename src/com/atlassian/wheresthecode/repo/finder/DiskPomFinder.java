package com.atlassian.wheresthecode.repo.finder;

import com.atlassian.fugue.Option;
import com.atlassian.wheresthecode.maven.MavenGav;
import com.google.common.base.Function;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import static java.lang.String.format;

public class DiskPomFinder {

    public FindResult findPomDotXml(MavenGav parentGav) {
        if (parentGav.getGroupId().isEmpty() && parentGav.getVersion().isEmpty()) {
            return new FindResult(Option.none(), false);
        }
        Option<File> pomDotXml = getPomFromHomeRepo(parentGav.getGroupId().get(), parentGav.getArtifactId(), parentGav.getVersion().get());
        return new FindResult(pomDotXml.map(getInputStreamFrom()), false);
    }

    private Function<File, InputStream> getInputStreamFrom() {
        return file -> {
            try {
                return new FileInputStream(file);
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
        };
    }

    Option<File> getPomFromHomeRepo(String groupId, String artifactId, String version) {
        File repo = new File(System.getProperty("user.home"), ".m2/repository");
        if (!repo.exists()) {
            return Option.none();
        }
        String groupIdPath = groupId.replaceAll("\\.", "/");
        File pom = new File(repo, format("%s/%s/%s/%s-%s.pom", groupIdPath, artifactId, version, artifactId, version));
        return pom.exists() ? Option.some(pom) : Option.none();
    }
}
