package com.atlassian.wheresthecode.repo.finder;

import com.atlassian.fugue.Option;

import java.io.InputStream;

public class FindResult {
    private final Option<InputStream> pomDotXml;
    private final boolean inLibraryCode;

    public FindResult(Option<InputStream> pomDotXml, boolean inLibraryCode) {
        this.pomDotXml = pomDotXml;
        this.inLibraryCode = inLibraryCode;
    }

    public Option<InputStream> getPomDotXml() {
        return pomDotXml;
    }

    public boolean isInLibraryCode() {
        return inLibraryCode;
    }

    public boolean isDefined() {
        return pomDotXml.isDefined();
    }
}
