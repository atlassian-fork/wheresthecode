package com.atlassian.wheresthecode.repo.finder;

import com.atlassian.fugue.Option;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectFileIndex;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.vfs.VfsUtilCore;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileVisitor;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.atomic.AtomicReference;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.option;

/**
 * Finds pom.xmls inside the IDEA Vfs and typically jars within that
 */
public class VfsPomFinder {

    public FindResult findPomDotXml(Project project, VirtualFile startFile) {
        if (startFile == null) {
            return new FindResult(Option.<InputStream>none(), false);
        }
        ProjectFileIndex projectFileIndex = ProjectRootManager.getInstance(project)
                .getFileIndex();

        boolean inLibraryClasses = projectFileIndex.isInLibraryClasses(startFile);
        if (inLibraryClasses) {
            // ok its a jar! style file
            VirtualFile classRoot = projectFileIndex.getClassRootForFile(startFile);
            Option<VirtualFile> pomDotXml = findManifestMavenPomFile(classRoot);
            return new FindResult(pomDotXml.map(this::toInputStream), true);
        } else {
            Option<VirtualFile> pomDotXml = findMavenPomFileFrom(startFile);
            return new FindResult(pomDotXml.map(this::toInputStream), false);
        }
    }

    private InputStream toInputStream(VirtualFile vf) {
        try {
            return vf.getInputStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Option<VirtualFile> findMavenPomFileFrom(VirtualFile startFile) {
        VirtualFile f = startFile;
        while (true) {
            if (f.isDirectory()) {
                Option<VirtualFile> pomDotXml = new VirtualPomVisitor(f).getPomDotXml();
                if (pomDotXml.isDefined()) {
                    return pomDotXml;
                }
            }
            f = f.getParent();
            if (f == null) {
                break;
            }
        }
        return none();
    }


    private Option<VirtualFile> findManifestMavenPomFile(VirtualFile root) {
        if (root != null) {
            VirtualFile metaInf = root.findChild("META-INF");
            if (metaInf != null) {
               return new VirtualPomVisitor(metaInf).getPomDotXml();
            }
        }
        return none();
    }

    public class VirtualPomVisitor {
        private AtomicReference<VirtualFile> pomDotXml = new AtomicReference<>();

        public VirtualPomVisitor(VirtualFile dir) {
            VfsUtilCore.visitChildrenRecursively(dir, new VirtualFileVisitor() {

                private boolean continueSearch = true;

                @Override
                public boolean visitFile(@NotNull VirtualFile file) {
                    if (!file.isDirectory() && file.getName().equals("pom.xml")) {
                        pomDotXml.set(file);
                        continueSearch = false;
                    }
                    return continueSearch;
                }
            });
        }

        Option<VirtualFile> getPomDotXml() {
            return option(pomDotXml.get());
        }

    }

}
